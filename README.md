# 🌐 hhajek.cz

Vítejte na oficiálním repozitáři webu [hhajek.cz](https://www.hhajek.cz/)!

## 📜 O nás

# 🚀 Pracovní zkušenosti

## Senior Backend Developer
**RVLT Digital** (červen 2024 - současnost)  
*6 měsíců +*  
*Hlavní město Praha, Czech Republic*


## PHP Programátor a Kodér
**NexGen IT, s.r.o.** (červen 2015 - červen 2024)  
*9 let*  
*District Ceske Budejovice, Czech Republic*

- Pracuji jako PHP programátor a kodér, zaměřuji se na vývoj webových aplikací a systémů.
- Zodpovědný za implementaci funkcí, údržbu a optimalizaci existujících systémů.
- Používám technologie PHP, JavaScript a další.

## External Full Stack Developer
**bicepsdigital** (září 2019 - současnost)  
*5 ket 3 měsíce +*  
*Hlavní město Praha, Česká republika*

- Pracuji jako externí full stack developer na projektech ve společnosti bicepsdigital.
- Specializuji se na vývoj frontendu a backendu pomocí technologií PHP, Symfony a dalších.

## Java Software Developer
**APA - Austria Presse Agentur** (únor 2023 - říjen 2023)  
*9 měsíců*  
*Na dálku*

- Vyvíjel jsem softwarové aplikace v Javě pro společnost APA - Austria Presse Agentur.
- Zkušenosti s PostgreSQL, Java a dalšími technologiemi.

## Frontend Developer
**Webpagesoftware Limited** (2010 - září 2015)  
*5 let 9 měsíců*

- Pracoval jsem jako frontend developer na různých projektech ve společnosti Webpagesoftware Limited.
- Specializoval jsem se na tvorbu uživatelských rozhraní a interaktivních prvků.

# 🎓 Vzdělání

## Bachelor's degree, Design webových aplikací
**Jihočeská univerzita v Českých Budějovicích** (2011 - 2014)

## Střední škola spojů a informatiky
**Informatika a výpočetní technika** (2007)


## 📚 Oblíbené kategorie

### 🛠️ Programování
- **PHP**
- **Java**
- **JavaScript**
- **Python**

### 🌐 Webový vývoj
- **HTML & CSS**
- **Vue.js**
- **React**

### 📊 Datová věda
- **Machine Learning**
- **Data Analysis**
- **Visualization**

## 📬 Kontakt

Máte-li jakékoliv dotazy nebo chcete-li se s námi spojit, neváhejte nás kontaktovat prostřednictvím našich sociálních sítí:

- **Email**: info@hhajek.cz
- **Twitter**: [@honzahajek91](https://x.com/honzahajek91)
- **LinkedIn**: [honza-hajek](https://www.linkedin.com/in/honza-hajek)

## 🎯 Cíl

Naším cílem je poskytovat kvalitní obsah, který pomáhá našim čtenářům růst a učit se nové věci v oblasti technologie a vývoje softwaru. Neustále pracujeme na nových článcích, tutoriálech a dalších zdrojích, které vám pomohou dosáhnout vašich cílů.

---

Děkujeme za návštěvu našeho repozitáře. Nezapomeňte se podívat na [HHajek.cz](https://www.hhajek.cz/) pro více informací a aktuální obsah!
